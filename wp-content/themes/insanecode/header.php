<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>
        <?php bloginfo('name');?>
    </title>
    <script class="img-logo" src="http://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <?php if(is_home()):
            $headerClass = "home-header";
          else:
            $headerClass =  "normal-header";
          endif; 

          if(is_admin_bar_showing()):
            $nav_class = "admin-nav";
          else:
            $nav_class = "";
          endif;
    ?>
       

    <!-- site header -->
    <header class="<?php echo $headerClass ?>">
        <a href="<?php echo home_url(); ?>"><img class="img-logo" src="<?php echo get_theme_file_uri('assets/img/logo.jpg') ?>" alt="logo"></a>
        <div class="brand">
            <h3><a href="<?php echo home_url(); ?>">La Unión<span> JPD</span></a> </h3>

        </div>
        <div class="navigation <?php echo $nav_class ?>">
            <!-- navigation menu -->
            <?php 
                   $args = array(
                       "theme_location" => "primary"
                   );  
                ?>
            <?php wp_nav_menu($args); ?>
            <!--navigation menu-->
            
        </div>
        <?php get_search_form() ?>
        <p class="toggle-search "> <i class="material-icons">search</i></p>
    </header>

    <!-- site header -->