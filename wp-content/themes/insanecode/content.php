<?php
  if (have_posts()) :
    while (have_posts()) : the_post();
  ?>

<article class="card-post wide-card">
  <div class="col-4">
    <img class="wide-card-image"src="<?php  the_post_thumbnail_url();?>" alt="">
  </div>
  <div class="col-6">
    <h1 class="post-title"><a class="a-title" href="<?php the_permalink()?>"><?php the_title()?></a></h1>
    <p class="post-info"> 
      <i class="post-date"> <i class="material-icons">date_range</i><?php the_time('F jS, Y g:i a'); ?> </i>
      <?php $categories = get_the_category();
            $separator = ", ";
            $output = "";
      foreach ($categories as $category):
        $output .= "<span> <a href='".get_category_link( $category->term_id )."'>". $category->cat_name . "</a></span>"; 
      endforeach;

      echo $output;
      ?>
  </p>
  <p><?php the_excerpt()?> </p>
  
  </div>
  
</article>

<?php  
    endwhile;
     echo paginate_links(); 
    else :
      echo "no contentent";
    endif;
       
?>