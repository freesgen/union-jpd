<?php get_header(); ?>

<section id="single">
		<?php if(have_posts()) : ?> <?php while(have_posts()) : the_post(); ?>

			<h1> <?php the_title();?> </h1> 
			<i id = "fecha"> <?php the_time('F jS, Y g:i a'); ?> </i>
			<a class = "link_categoria" href="<?php the_permalink() ?>" >  <?php the_category(); ?> </a>
			<hr id = "separador"/>
		<div id = "texto">  
			<?php the_content(); ?>
		</div>
	<?php endwhile; ?>
	<?php endif; ?>	
		
</section>

<?php comments_template() ?>

<div class="fixed-side-bar">
	<?php get_sidebar() ?>
</div>


<?php get_footer(); ?>