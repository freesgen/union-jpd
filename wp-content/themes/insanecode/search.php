
<?php
  get_header();
?>

<section class="page-section search-section">

  <div class="results">
    <h2 class="section-title">Resultados para: "<?php echo get_search_query() ?>"</h2>
    <?php get_template_part("content"); ?>
  </div>

  <?php get_sidebar( );?>

</section>

<?php   
 
  get_footer();

?>