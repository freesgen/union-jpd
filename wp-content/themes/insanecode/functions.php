<?php
  $myPath = "";
  function Insane_HeadResources(){

    wp_enqueue_style("style", get_stylesheet_uri());
    wp_enqueue_style( "icons", "https://fonts.googleapis.com/icon?family=Material+Icons" );

  }

  add_action("wp_enqueue_scripts","insane_headResources");

  function insane_jScriptResources(){
    wp_enqueue_script( "myslider", get_theme_file_uri( "assets/js/myslider.js"), array(),$ver = true, $is_footer = true );
    wp_enqueue_script( "myfunctions", get_theme_file_uri( "assets/js/myFunctions.js"), array(), $ver = true, $is_footer = true );
  }


    
    add_action("wp_enqueue_scripts","insane_jScriptResources");

  
    // custom excerpt

    function custom_excerpt(){
      return 20;
    } 

    add_filter( 'excerpt_length',custom_excerpt);


    
    function myPage_setup(){
      
    // nav menu support

    register_nav_menus(array(
      "primary" => __("Primary Menu"),
      "footer" => __("Footer Menu"),
    ));


    // add featured image support
      add_theme_support("post-thumbnails");

    }
    

    add_action( "after_setup_theme", myPage_setup );


    function my_page_login(){
      wp_enqueue_style("style_login", get_stylesheet_directory_uri() . '/login.css');
      wp_enqueue_script( "mylogin", get_theme_file_uri( "assets/js/login.js"),  array("jquery") );
    }

   

    add_action('login_enqueue_scripts','my_page_login');


    // footer content editable 1

    function insane_middle_footer($wp_customize){
      $wp_customize->add_section("insane_middle_footer_section", array(
        "title" => "Middle Footer"
      ));

      $wp_customize->add_setting("insane_middle_footer_title",array(
        "default" => "¿Quienes Somos?"
      ));

      $wp_customize->add_control(new WP_Customize_Control($wp_customize,
      "insane_middle_footer_title_control",array(
        "label"     => "Titulo",
        "section"   => "insane_middle_footer_section",
        "settings" => "insane_middle_footer_title"
      )));


       $wp_customize->add_setting("insane_middle_footer_body",array(
        "default" => "La Union JPD , es una junta de vecinos de la comunidad de
                      Villa Hermosa ubicada en la ciudad de La Romana, hemos trabajado 
                      por el bien de nuestro municipio y comunidad desde el año 2007"
      ));

      $wp_customize->add_control(new WP_Customize_Control($wp_customize,
      "insane_middle_footer_body_control",array(
        "label"     => "Cuerpo",
        "section"   => "insane_middle_footer_section",
        "settings" => "insane_middle_footer_body",
        "type" => "textarea"
      )));
    }

    add_action( 'customize_register', 'insane_middle_footer');


    // donations

    function insane_donations($wp_customize){
      $wp_customize->add_section("insane_donations_section", array(
        "title" => "Donaciones Info"
      ));

      $wp_customize->add_setting("insane_donations_bank",array(
        "default" => "BanReservas"
      ));

      $wp_customize->add_control(new WP_Customize_Control($wp_customize,
      "insane_donations_bank_control",array(
        "label"     => "Banco",
        "section"   => "insane_donations_section",
        "settings" => "insane_donations_bank"
      )));

      $wp_customize->add_setting("insane_donations_bank_acount",array(
        "default" => "numero de cuenta"
      ));

      $wp_customize->add_control(new WP_Customize_Control($wp_customize,
      "insane_donations_bank_acount_control",array(
        "label"     => "Cuenta de Banco",
        "section"   => "insane_donations_section",
        "settings" => "insane_donations_bank_acount"
      )));

    }

    add_action( 'customize_register', 'insane_donations');

    //
    // slider images

    function insane_slide($wp_customize){
      $wp_customize->add_section("insane_slide_section", array(
        "title" => "Slide Show"
      ));

      $wp_customize->add_setting("insane_slide_image1",array(
        
      ));

      $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,
      "insane_slide_image1_control",array(
        "label"     => "Imagen 1",
        "section"   => "insane_slide_section",
        "settings" => "insane_slide_image1"
      )));

      $wp_customize->add_setting("insane_slide_image2",array(
        
      ));

      $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,
      "insane_slide_image2_control",array(
        "label"     => "Imagen 2",
        "section"   => "insane_slide_section",
        "settings" => "insane_slide_image2"
      )));

      $wp_customize->add_setting("insane_slide_image3",array(
        
      ));

      $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,
      "insane_slide_image3_control",array(
        "label"     => "Imagen 3",
        "section"   => "insane_slide_section",
        "settings" => "insane_slide_image3"
      )));


    }

    add_action( 'customize_register', 'insane_slide');
   
//  desactiva la basura del header -->
//  desactiva la basura del header -->

function theme_cleanup() {
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
  remove_action( 'wp_head', 'print_emoji_detection_script', 7);
  remove_action( 'wp_print_styles',     'print_emoji_styles' );
}
add_action('after_setup_theme', 'theme_cleanup', 16);

/**
 * Removes WordPress version from scripts
 */
function theme_remove_version_code($src) {
	if (strpos($src, 'ver=') !== false) {
		$src = remove_query_arg('ver', $src);
	}
	return $src;
}
add_filter('style_loader_src', 'theme_remove_version_code', 99);
add_filter('script_loader_src', 'theme_remove_version_code', 99);
//  desactiva la basura del header -->

//  desactiva la basura del header -->
    
    
?>