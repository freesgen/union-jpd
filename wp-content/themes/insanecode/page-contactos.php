
<?php
  get_header();
?>

<section class="page-section">

<?php
  if (have_posts()) :
    while (have_posts()) : the_post(); ?>
  <article class="post-article">
  <h1><?php the_title()?></h1>
  <div class="card-list-container">
    <div class="half center contacts-links">  
      

      <h2 id="c-fb" class="contact-item"><span class="net">FB:</span> 
      <span class="net-desc">cuenta #<?php echo get_theme_mod("insane_donations_bank_acount"); ?></span></h2>

      <h2 id="c-email"class="contact-item"><span class="net">EMAIL:</span> 
      <span class="net-desc">cuenta #<?php echo get_theme_mod("insane_donations_bank_acount"); ?></span></h2>

      <h2 id="c-twitter" class="contact-item"><span class="net">Twitter:</span> 
      <span class="net-desc">cuenta #<?php echo get_theme_mod("insane_donations_bank_acount"); ?></span></h2>

      <h2 id="c-twitter" class="contact-item"><span class="net">Tel:</span> 
      <span class="net-desc">829-327-1958</span></h2>



    </div>
    <div class="half center">
      <img class="img-big-logo" src="<?php echo get_theme_file_uri('assets/img/logo.jpg') ?>" alt="logo">
    </div>

  </div>

  </article>
  
  <?php
    endwhile;
    else :
      echo "no contentent";
    endif;
   
?>

</section>

<section class="donations page-section" id="donaciones">
  <h1 class="section-title">Donaciones</h1>

  <div class="card-list-container">
    <div class="half some-words">
    <p>Trabajamos con un conjunto de colaboradores que nos ayudan a hacer las actividades
      de limpieza , para niños y ancianos, tu puedes formar parte de esa ayuda, si estas en la disposicion puedes
      ponerte en contacto con nosotros para mas información o depositar tu ayuda a esta cuenta.
     </p>

     <p>De todos modos nos encantaria leer tu opinion acerca de lo que podemos mejorar en cualquiera de nuestros posts
      puedes dejar un comenentario o llamanos para mas info
     </p>
    
    </div> 

    <div class="half center info-acount">
      <h1 class="sub-section-title">Ayudanos a seguir trabajando</h1>

      <h2 class="charity-box"><span class="bank"><?php echo get_theme_mod("insane_donations_bank"); ?>:</span> 
       <span class="bank-acount">cuenta #<?php echo get_theme_mod("insane_donations_bank_acount"); ?></span></h2>
    </div> 
  
  </div>

</section>

<?php   

 
 get_footer();  
?>