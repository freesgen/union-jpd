<?php
  get_header();
?>

  <section class="main-section">
    

    <article class="slide">
      <div src="" alt="" class="slider-background"></div>
      <div class="slide-show">
          <div class="slide-item"> <img class="slide-img" src="<?php echo get_theme_mod('insane_slide_image1'); ?>"/></div>
          <div class="slide-item"> <img class="slide-img" src="<?php echo get_theme_mod('insane_slide_image2'); ?>"/></div>
          <div class="slide-item"> <img class="slide-img" src="<?php echo get_theme_mod('insane_slide_image3'); ?>"/></div>
           
      </div>

      <div class="slide-text">
        <div class="half a-center">
          <h1 class="slide-title">Para vivir en una mejor Comunidad
              Debemos estar unidos...</h1>
        </div>
        <div class="half a-down">
          <a href="<?php echo site_url('contactos/#donaciones') ?>" class="btn over-transparent"> Donaciones</a>
        </div>
      </div>
      <ul class="slide-indicator">
        <li class="indicator-item"><a href="" rol="low-indicator"></a></li>
        <li class="indicator-item"><a href="" rol="low-indicator"></a></li>
        <li class="indicator-item"><a href="" rol="low-indicator"></a></li>
      </ul>
    </article>

    <div class="sub-section">
      <h1 class="sub-section-title">Actividades</h1>
      <div class="card-list-container">
        <?php

          $generalPosts = new WP_Query('cat=8&posts_per_page=3');

          if ($generalPosts->have_posts()) :
            while ($generalPosts->have_posts() ) : 
              $generalPosts->the_post(); 
                 ?>
                <article class="card-post">
                  <h2 class="post-title"> <?php the_title()?></h2>
                  <i class="post-date"><i class="material-icons">date_range</i> <?php the_time('F jS, Y g:i a'); ?> </i>
                  <i class="category-tag"><?php echo $generalPosts->get_categories[0] ?></i>
                  <p> <?php echo get_the_excerpt();?> </p>
                  <a href="<?php the_permalink() ?>" class="btn read-more"> Lee el articulo</a>
                </article>
                <?php
            endwhile;
            else :
              echo "no contentent";
          endif;
          wp_reset_postdata();
        ?>
      </div>

      <a href="<?php echo get_category_link(8); ?>" class="btn over-light a-right">Ver Actividades</a>
    </div>

    
    
    <div class="sub-section stared-article">
      <h1 class="sub-section-title">Articulo Destacado</h1>

        <?php
            
            $beautifyingPosts = new WP_Query("cat=14&posts_per_page=1");
             if ($beautifyingPosts->have_posts()) :
              while ($beautifyingPosts->have_posts()): 
                $beautifyingPosts->the_post();               
                ?>
                <article class="stared-post">
                  <img src="<?php  the_post_thumbnail_url();?>" alt="">
                  <div class="text-card half">
                  <h2 class="post-title"><?php the_title() ?></h2>
                  <i class="post-date"><i class="material-icons">date_range</i> <?php the_time('F jS, Y g:i a'); ?> </i>
                  <p> <?php echo get_the_excerpt();?></p>
                   
                   <a href="<?php the_permalink() ?>" class="btn over-light-inverted">Leer Articulo</a>
                </div>
                </article>
                
                <?php
              endwhile;
              endif;
              wp_reset_postdata();
            ?>

      
     
    </div>

    <div class="sub-section">
      <h1 class="sub-section-title">Embellecimiento</h1>
      <div class="card-list-container">
        <?php
            
            $beautifyingPosts = new WP_Query("cat=9&posts_per_page=3");
             if ($beautifyingPosts->have_posts()) :
              while ($beautifyingPosts->have_posts()): 
                $beautifyingPosts->the_post();               
                ?>
                <article class="img-posts">
                  <img src="<?php  the_post_thumbnail_url();?>" alt="">
                  <h2 class="post-title-prev"><?php the_title() ?></h2>
                  <a href="<?php the_permalink() ?>"><div class="img-post-detail">
                    <h2 class="post-title"><?php the_title() ?></h2>
                    <i class="post-date"><i class="material-icons">date_range</i> <?php the_time('F jS, Y g:i a'); ?> </i>
                    <p> <?php echo get_the_excerpt();?></p>
                  </div></a>
                </article>
                <?php
              endwhile;
              endif;
              wp_reset_postdata();
            ?>
      </div>

      <a href="<?php echo get_category_link(9);?> "class="btn over-light-inverted a-right">Ver Todas</a>
     
    </div>

     <div class="separator-container"><div class="separator"></div></div>

    <div class="sub-section vcenter">
      <h1 class="sub-section-title">Asesoría</h1>
      <div class="p-centered-content">
        <p>Nosotros podemos ayudarte con asesorias en bla bla bla \n sda;djas ;dkaslkdaslkddasldkj aslk jdas jdkla sdkljas
          slakdj lkajsdkjaskdja dkjas dkjaksjdaksj dkajs dkjas da djklas jkdj askdjaksj dkjasdjsak jdksajdkjas kdj askjd
          alskjdkalsjd kaj sdkj askdj askjdlkajsdkjaskdjask jdaksjdkljaskjd kasjdkjask djkasjdkajskdj akjsdkjaskdj lkasjdklajs
          alskdjlkasjkdj ad ask jdkalsjdkajs daskjd aksjdkjas da dkasjdkjaskjd aslkjd ask dlkasj dkasj daksjdklaskdl aksd jakls
          askjdla jslkjdaksjdkajakl sjdkajdsak
        </p>
      </div>

      <a class="btn over-light">Asesoria</a>
    </div>

  </section>

  <?php get_footer(); ?>