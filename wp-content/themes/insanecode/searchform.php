

<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
    <div>
        <label class="screen-reader-text" for="s">Buscar:</label>
        <input type="text" value="<?php echo get_search_query() ?>" name="s" id="s" />
        <input type="submit" id="searchsubmit" value="Buscar" />
        <img class="search-img" src="<?php echo get_theme_file_uri('assets/img/ic_search.svg') ?>"/>
    </div>
</form>