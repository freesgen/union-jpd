  
  <footer class="site-footer media-row  row-6">
    <div class="bigfooter">
      <article class="page-info col-3">
        <h2 class="foot-section-title">La Union JPD</h2>
        <img src="<?php ?>" alt="">
        <q>Unidos podemos mas</q>

        
        <div class="redes">
         <?php if(!is_page("contactos")): ?>
          <img class="social-icons" src="<?php echo get_theme_file_uri('assets/img/social_fb.svg') ?>" alt="twitter">
          <img class="social-icons" src="<?php echo get_theme_file_uri('assets/img/social_twitter.svg') ?>" alt="twitter">
          <img class="social-icons" src="<?php echo get_theme_file_uri('assets/img/social_mail.svg') ?>" alt="twitter">
        
        <?php else:
        
        
          endif; ?>
        </div>
        <div class="webmaster">
        <p>Website by Jesus Guerrero &amp; Tommy Suazo </p>
        <P>"programming for passion"</P></div>

      </article>

      <article class="about-page col-3 ">
        <h2 class="foot-section-title"><?php echo get_theme_mod("insane_middle_footer_title"); ?></h2>

          <p><?php echo get_theme_mod("insane_middle_footer_body"); ?></p>

          <a class="btn btn-white" href="<?php echo admin_url(); ?>">Login</a>
      </article>

      <article class="foot-menu-container col-3">
      <h2 class="foot-section-title">Categorias</h2>
         <!-- footer navigation menu -->
            <?php 
                   $args = array(
                       "theme_location" => "footer"
                   );  
                ?>
            <?php wp_nav_menu($args); ?>
            <!--footer navigation menu-->
          
      </article>
    </div>
   <p class="copyright-info"> <?php bloginfo("name"); echo " - &copy; ".date("Y"); ?> | All right reserved | Code and Design by Insane Code</p>
  </footer>

  <?php wp_footer(); ?>
 
  </body>
</html>