// Jesus Guerrero 4 mar 2017


jQuery(document).ready(function(){

 var $ = jQuery,
     $navButtons = $(".navigation").find("a");



  slideFunctions();

  function slideFunctions(){
    var $slideShow      =  $(".slide-show"),
        $slideImg       =  $(".slide-img"),
        $sliderBackground = $(".slider-background"),
        $slideItem      =  $(".slide-item"),
        $indicator      =  $("[rol='low-indicator']"),

        slideLoopCount  =  0,
        slideOffset     =  0;
        
    const SLIDE_TIME = 10000;

    initSlider();
    changeSlide();

    // funciones

    function initSlider(){

      $navButtons.on('mouseover',blurSlider);
      $navButtons.on('mouseleave',quitSliderBlur);

    }

    function changeSlide(){
      setInterval(change,SLIDE_TIME);
      $indicator.eq(slideLoopCount).animate({width:"100%"},SLIDE_TIME);
      $slideImg.eq(slideLoopCount).animate({"filter":"blur(30px)"},SLIDE_TIME);

      var imagesSources = [],
          i = 0;

      $slideItem.each(function(){
        var $this = $(this);
        imagesSources[i] = $this.find("img").attr("src");  
        i++;
      });

      $sliderBackground.css({backgroundImage:"url(" + imagesSources[slideLoopCount] + ")",});

      function change(){
        if(slideLoopCount < $slideImg.length - 1){
          slideLoopCount++;
          slideOffset -= 100;
          $slideShow.animate({left: slideOffset + "vw"},1000);
          $indicator.eq(slideLoopCount).animate({width:"100%"},SLIDE_TIME);
          $sliderBackground.css({backgroundImage:"url(" + imagesSources[slideLoopCount] + ")",});
          
        }else{
          slideLoopCount = 0 ;
          slideOffset = 0;
          $slideShow.animate({left: slideOffset},1000);
          $indicator.animate({width:"1%"},200);
          $indicator.eq(slideLoopCount).animate({width:"100%"},SLIDE_TIME);
        }         

      }
      
    }


    function blurSlider(){
     $slideShow.addClass("blur-slide");
    }

    function quitSliderBlur(){
     $slideShow.removeClass("blur-slide");
    }
  }
  //  end slidefunctions


  
})