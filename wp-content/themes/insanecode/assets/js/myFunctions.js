jQuery(document).ready(function(){

  var $ = jQuery;
  const   $header = $("header"),
          $brand = $(".brand"),
          $navigation = $(".navigation"),
          $firstArticle = $(".sub-section"),
          $toggleSearch = $(".toggle-search"),
          $searchIcon = $toggleSearch.find("i");
      

  var FAOffset = 5,
      ExpandedMenu = false,
      toggleSearchPadding = "145px",
      windowWidth,
      iconValue="search",
      searchFormSize= "70%";
    



  headerFunctions();
  searchFunctions();


  // functions definitions



    function headerFunctions(){
      

      adaptScreenWidth();
  
       $(window).resize(function(){
        adaptScreenWidth()
      });  
              
       // adapto los valores segun el tamaño de la pantalla
      function adaptScreenWidth(){
         
        windowWidth = window.innerWidth
        if(windowWidth <= 768) {
          toggleSearchPadding = "10px";
          iconValue = "menu";
          $searchIcon.text(iconValue); 
          searchFormSize = "86%"
        }
        else if(windowWidth > 768) {
          toggleSearchPadding = "15px";
          iconValue = "search";      
          $searchIcon.text(iconValue);
          searchFormSize = "70%";
        } 
      }

      
    }

    // funciondes de la barra de busqueda
    function searchFunctions(){

      var $searchText = $("#s"),
          $searchForm = $("[role='search']"),
          $searchButton =$("#searchsubmit");

      var SearchExpanded = false;


      $searchText.attr("placeholder","Escribe y presiona enter para buscar");
      
      $toggleSearch.on('click',function(){
        
        if(!SearchExpanded){
            $searchForm.css({"display":"flex"});
            $searchForm.animate({"width":searchFormSize},1000,function () {
              $searchText.focus();
            });
            showNav();

            $toggleSearch.animate({right:"10px"},500,function () {  
              $toggleSearch.find("i").text("clear");
              SearchExpanded = true;
            });
            
        }else{
          hideNav();
          $searchForm.animate({"width":"0"},1000,function () {  
            $toggleSearch.animate({right:toggleSearchPadding},500);
            $searchForm.css({"display":"none"});
            $toggleSearch.find("i").text(iconValue);
            SearchExpanded = false;
          });
        }
          
      });  

    }



    function showNav() {
      if(windowWidth <= 768){
        $navigation.addClass("showed-menu");
        $navigation.animate({"right":"0"},600)
      }
    }

    function hideNav() {
      if(windowWidth <= 768){
        $navigation.animate({"right":"-60%"},600,function(){
          $navigation.removeClass("showed-menu");
        })
        
        
      }
    }



});