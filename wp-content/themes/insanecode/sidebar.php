<aside id="sidebar">
   <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar Widgets')) : else : ?>
      <div class="static-widget">
        <h2>Frase</h2>
        <p>
        Lorem ipsum dolor sit amet, consectetur elit porta. Vestibulum ante justo, volutpat quis porta non,
        vulputate id diam. Lorem et ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae sem in massa 
        sagittis.
        </p>
      
      </div>  
      
      <div class="static-widget">
        <h2>Archivos</h2>
    	  <ul>
    	     <?php wp_get_archives('type=monthly'); ?>
    	  </ul>
      </div>

     <div class="static-widget">
      <h2>Categorias</h2>
        <ul>
    	   <?php wp_list_categories('show_count=1&title_li='); ?>
        </ul>

    	<?php wp_list_bookmarks(); ?>
     
     </div>
     
     <div class="static-widget">
        <h2>Meta</h2>
    	  <ul>
    	    <?php wp_register(); ?>
    	    <li><?php wp_loginout(); ?></li>
    	  <?php wp_meta(); ?>
    	</ul>
     </div>
     

	<?php endif; ?>
</aside>