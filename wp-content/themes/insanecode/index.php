
<?php
  get_header();
?>

<section class="page-section">

<?php
  if (have_posts()) :
    while (have_posts()) : the_post(); ?>
  <article class="post-article">
  <h1><a href="<?php the_permalink()?>"><?php the_title()?></a></h1>
  <p class="post-info"> 
    Publicado Por <span class="post-author"> <?php the_author();?></span>
      <i class="post-date"> --<?php the_time('F jS, Y g:i a'); ?> </i>
      <?php $categories = get_the_category();
            $separator = ", ";
            $output = "";
      foreach ($categories as $category):
        $output .= "<span> <a href='".get_category_link( $category->term_id )."'>". $category->cat_name . "</a></span>"; 
      endforeach;

      echo $output;
      ?>
  </p>

  <p><?php the_content()?> </p>
  </article>
  
  <?php
    endwhile;
    else :
      echo "no contentent";
    endif;
   
?>

</section>
<div class="comments-container">
  <?php  comments_template( ); ?>
</div>
<?php   

 
 get_footer();  
?>